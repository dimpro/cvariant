
# cvariant
this library appends dynamic type variable to C lang

##### how to use

example:

``` C

#include "cvariant/cvariant.h"

#include <stdio.h>
#include <stdlib.h>


int main()
{
  //--- first example ---
  //you can use variant_new() function
  //for allocate a memory and clean it
  variant_t * var1 = variant_new();
  var1->val_type = variant_float;
  var1->val.val_float = 1.2;

  //you can do by your self
  variant_t * var2 = malloc(sizeof(variant_t));
  variant_zero(var2);
  var2->val_type = variant_int8;
  var2->val.val_int8 = 3;

  variant_t ret;
  ret.val_p = NULL;
  variant_op(variant_add, &ret, var1, var2);

  //after operation, the ret is float type now
  printf("ret = %f\n", ret.val.val_float);
  
  //--- second example ---
  //you can use reference to another variant_t variable
  variant_t var3;
  //variant_zero() just sets to zero all bytes,
  //sets type to variant_uint64 and sets reference pointer to NULL
  //you can do it by the hands
  var3.val.val_uint64 = 0;
  var3.val_type = variant_int16; //or any other type
  var3.val.val_int16 = 300;
  var3.val_p = NULL;
  //if this reference pointer not NULL,
  //the library thinks, this variant_t is a reference to another variant_t

  //sets make our var1 as a reference to var3
  var1->val_p = &var3;

  variant_op(variant_add, &ret, var1, var2);
  printf("ret = %d\n", ret.val.val_int16);

  //--- third example ---
  //you can dereference variable to look at the referenced variable
  printf("this is var1: %f\n", var1->val.val_float);
  printf("this is dereferenced var1: %d\n",
      variant_dereference(var1)->val.val_int16);

  return 0;
}

```

output:

ret = 4.200000
ret = 303
this is var1: 1.200000
this is dereferenced var1: 300

