
#ifndef _CVARIANT_H_
#define _CVARIANT_H_

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef union variant_val_t variant_val_t;
typedef struct variant_t variant_t;

union variant_val_t
{
    int8_t val_int8;
    uint8_t val_uint8;
    int16_t val_int16;
    uint16_t val_uint16;
    int32_t val_int32;
    uint32_t val_uint32;
    int64_t val_int64;
    uint64_t val_uint64;
    float val_float;
    double val_double;
};

enum variant_type_t
{
    variant_int8,
    variant_uint8,
    variant_int16,
    variant_uint16,
    variant_int32,
    variant_uint32,
    variant_int64,
    variant_uint64,
    variant_float,
    variant_double,
    variant_type_count
};

enum variant_operation_t
{
    variant_add,
    variant_minus,
    variant_mul,
    variant_div,
    variant_mod,
    variant_and,
    variant_or,
    variant_xor,
    variant_shl,
    variant_shr,
    variant_op_count
};

struct variant_t
{
    variant_val_t val;
    enum variant_type_t val_type;
    variant_t * val_p;
};

//---Main functions---

variant_t * variant_new();
void variant_zero(variant_t * val);
int variant_op(enum variant_operation_t op, variant_t * ret, variant_t * l, variant_t * r);

//---Additional functions---

//clean the non using bytes in the value storage union
void variant_clean(variant_t * val);
variant_t variant_cast_to_type(variant_t * var, enum variant_type_t type);
variant_t * variant_dereference(variant_t * val);


#ifdef __cplusplus
}
#endif


#endif //_CVARIANT_H_

